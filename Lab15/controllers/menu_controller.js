const Pieza = require('../models/piezas_ropa');



exports.getList = (request, response, next) => {
    console.log('Ruta /menu/ropa');
    //console.log(request.get('Cookie').split(';')[1].trim().split('=')[1]);
    //console.log(request.cookies);
    //console.log(request.cookies.ultimo_platillo);
    response.render('lista_ropa', {
        titulo: "Ropa",
        lista_piezas: Pieza.fetchAll()

    });
};

exports.getAdd = (request, response, next) => {
    response.render('add_ropa',{
        titulo: "Agregar nueva pieza (sueter)",
    });
};

exports.postAdd = (request, response, next) => {
    //response.setHeader('Set-Cookie', 'ultimo_platillo= '+ request.body.nombre+'; HttpOnly');
    const pieza = new Pieza(request.body.nombre, request.body.precio, "https://m.media-amazon.com/images/I/61GnLXLVOkL._AC_SY550_.jpg" );
    pieza.save();
    response.status(302).redirect('/menu/ropa');
    
};