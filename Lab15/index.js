const express = require('express');
const app = express();


const bodyParser = require('body-parser');
const rutasMenu = require('./routes/menu');
const rutasFutbol = require('./routes/futbol');

const path = require('path');
const router = express.Router();

//Middleware
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({extended: false}));

app.use('/menu', rutasMenu);


app.get('/index', (request, response, next) => {
    response.sendFile(path.join(__dirname, 'views', 'index.html'));
});

app.use('/futbol', rutasFutbol);
app.use('/menu', rutasMenu);


app.use((request, response, next) => {
    console.log('Segundo middleware!');
    response.status(404).send('Recurso no encontrado :('); //Manda la respuesta
});

app.listen(3000);