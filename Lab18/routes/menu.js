const express = require('express');

const router = express.Router();

const menuController = require('../controllers/menu_controller');



router.get('/ropa/:pieza_id', menuController.getList);


router.get('/ropa', menuController.getList);
router.get('/add', menuController.getAdd);
router.post('/add', menuController.postAdd);


module.exports = router;