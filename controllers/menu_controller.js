const { request, response } = require('express');
const Platillo = require('../models/platillo');
const platillos = require('../models/platillo');


exports.getList = (request, response, next) => {
    console.log('Ruta /menu/list');
    //console.log(request.get('Cookie').split(';')[2].trim().split('=')[1]);
    console.log(request.cookies);
    console.log(request.cookies.ultimo_platillo);
    Platillo.fetchAll(request.params.platillo_id)
        .then(([rows, fieldData]) => {
        console.log(rows);
        response.render('lista_menu', {
            titulo: "Menú",
            isLoggedIn: request.session.isLoggedIn,
            username: request.session.username,
            lista_platillos: rows,
            //lista_platillos: platillos.fetchAll()
        });
    }).catch(err => {
        console.log(err);
        response.status(302).redirect('/error');
    });
};

exports.getAdd = (request, response, next) => {
    
    response.render('add_menu',{
        titulo: "Agregar platillo al menú",
        isLoggedIn: request.session.isLoggedIn,
        username: request.session.username,
    });
};

exports.postAdd = (request, response, next) => {
    console.log(request.body);
    console.log(request.body.nombre);
    response.setHeader('Set-Cookie', 'ultimo_platillo= '+ request.body.nombre+'; HttpOnly');
    const platillo = new Platillo(request.body.nombre, request.body.descripcion, "https://i.blogs.es/87930e/comidas-ricas/650_1200.jpg" );
    platillo.save()
    .then(() => {
        response.status(302).redirect('/menu/list');
        });
    }).catch(err => {

        response.status(302).redirect('/error');
    });
    
    
};
}
exports.postSearch = (request,response, next) =>{
    response.status(200).json({message: "Respuesta asíncrona"});
}