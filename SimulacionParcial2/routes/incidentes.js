const express = require('express');

const router = express.Router();

const incidentesController = require('../controllers/incidentes_controller');

//router.get('/list/:incidente_id', incidentesController.getListId);

router.get('/ver-incidentes', incidentesController.getIncidentes);

router.get('/new-incidente', incidentesController.getNewIncidentes);

router.post('/new-incidente', incidentesController.postNewIncidentes);

//router.post('/search', incidentesController.postSearch)

module.exports = router;