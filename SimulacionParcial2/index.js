const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const path = require('path');
const router = express.Router();

const rutasIncidentes = require('./routes/incidentes');

/*app.use(bodyParser.urlencoded({extended: false}));

app.use('/alguna-ruta', (request, response, next) => {
    console.log(request.body);
}); */
   

//Middleware
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.get('/index', (request, response, next) => {
    response.sendFile(path.join(__dirname, 'views', 'index.html'));
});

app.use('/incidentes', rutasIncidentes);

app.get('/', (request, response, next) => {
    response.send('Bienvenido a el apartado de Incidentes JURASIC PARK - ir a localhost3000/incidentes/incidentes');
});

app.use((request, response, next) => {
    console.log('Primer Middleware!');
    console.log(request.body);
    next(); //Le permite a la petición avanzar hacia el siguiente middleware
});


app.use((request, response, next) => {
    console.log('Segundo middleware!');
    response.status(404).send('¡Recurso no encontrado!'); //Manda la respuesta
});



app.listen(3000);