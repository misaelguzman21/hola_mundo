//Ejercicio 1
let suma=0
const arreglo = [36, 65, 43, 12, 22, 35, 66, 120];
let lon = arreglo.length;

for (let i of arreglo){
	suma= suma+i;
	//suma+=i;
}
prom= suma/lon;
console.log(prom);

//Ejercicio 2
function recibirString(stringRecibido){
	const fs = require('fs');
	fs.writeFileSync("lab9.txt", stringRecibido);
}
recibirString("Tengo mucho sueño :(");

//Ejercicio 3
function transpose(matrix, jLength){
    const resultado = [];
    for(let i = 0; i<matrix.length;i++){
        resultado.push([]);
    }

    for(let i = 0; i< matrix.length;i++){
        for(let j = 0; j<jLength;j++){
            resultado[j].push(matrix[i][j]);
        }
    }
    return resultado;
}

const matrix = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9]
  ]
const despues = transpose(matrix,3);
console.log(despues);



const fs = require('fs')
const http = require('http')

fs.readFile('Desktop/hola_mundo/index.html', function (err, html) {
    if (err) {
        throw err;
    }
    const server = http.createServer((request, response) => {
        // magic happens here!

        console.log(request.url);
        response.setHeader('Content-Type', 'text/html');
        response.write(html);
        response.end();
    })
    server.listen(3000);
});
