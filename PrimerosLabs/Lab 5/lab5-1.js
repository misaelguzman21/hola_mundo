/*
Lab 5 Ejercicio 1
Entrada: un número pedido con un prompt. 
Salida: Una tabla con los números del 1 al número dado con sus cuadrados y cubos. 
Utiliza document.write para producir la salida
*/


const numero = window.prompt("Digita un número: ");
document.write("<h2>Ejercicio 1: </h2>")
for(var i = 1; i<= numero; i++){
	document.write("<table><tr><th>Número</th><th>Cuadrado</th><th>Cubo</th></tr><tr><td>"+ i +"</td><td>"+ i**2 +"</td><td>"+ i**3 +"</td></tr></table></table>");

}


/*
Lab 5 Ejercicio 2
Entrada: Usando un prompt se pide el resultado de la suma de 2 números generados de manera aleatoria. 
Salida: La página debe indicar si el resultado fue correcto o incorrecto, y el tiempo que tardó el 
usuario en escribir la respuesta.
*/


	let num1 = Math.round(Math.random()*1000);
	let num2 = Math.round(Math.random()*1000);
	let tiempo_incial = new Date();
	let tiempo_total;

	const resultado = num1 + num2;

	const resultado_usuario = window.prompt("Digite el resultado de la suma de " + num1 + "+" + num2);

	if(resultado === parseInt(resultado_usuario)){
		let tiempo_final = new Date();
    	tiempo_total = (tiempo_final - tiempo_incial)/1000;
		document.write("Resultado correcto, " + num1 + "+" + num2 + " es " + resultado + "!!" + ", Te tomó: " + tiempo_total + " segundos contestar...");
	}
	else{
		let tiempo_final= new Date();
    	tiempo_total = (tiempo_final - tiempo_incial)/1000;
		document.write("¡Resultado incorrecto! , " + num1 + " + " + num2 + " es " + resultado + "  :S" + ", te tomó: " + tiempo_total +  " segundos contestar...");
	}


/*
Lab 5 Ejercicio 3
Función: contador. Parámetros: Un arreglo de números. 
Regresa: La cantidad de números negativos en el arreglo, la cantidad de 0's, y la cantidad de valores mayores a 0 en el arreglo.
*/

let num_ceros = 0; 
let positivos = 0; 
let negativos = 0;

function getRandomNumbers(max, min){
	return Math.floor(Math.random() * (max - min)) + min;
}

let arreglo = [];

for(let i = 1 ; i <= 30 ; i++ ){
	arreglo.push(getRandomNumbers(50,-50));
}



function contador(arreglo){

	for (let i = 0; i < arreglo.length; i++){
		if (arreglo[i] === 0){
			num_ceros++;
		}else if (arreglo[i] < 0 ){
			negativos++;
		}else {
			positivos++;
		}

	}
	console.log(arreglo);
    console.log("Hay ", num_ceros, " ceros");
    console.log("Hay ", negativos, " numeros negativos");
    console.log("Hay ", positivos, " numeros positivos");


}

contador(arreglo);



/*
Lab 5 Ejercicio 4
Función: promedios. Parámetros: Un arreglo de arreglos de números. 
Regresa: Un arreglo con los promedios de cada uno de los renglones de la matriz.
*/

function promedioArr(){
   
    function promedios(array){
        let tmp = 0;

        for(let i = 0; i < 3; i++){
            for(let j = 0; j < 3; j++){
                tmp += array[i][j];
            }
            console.log("El promedio del reglon " + (i+1) + " del arreglo es: " + (tmp / 3) + ".");
        }
    }

    let arr = [[1,2,3], [4,5,6], [7,8,9]];
    console.log("Arreglo a promediar: " + arr);
    promedios(arr);

    let arra = [[10,20,30], [40,50,60], [70,80,90]];
    console.log("Segundo Arreglo a promediar: " + arra);
    promedios(arra);


}


/*
Lab 5 Ejercicio 5
Función: inverso. Parámetros: Un número. Regresa: El número con sus dígitos en orden inverso.
*/

let num_original = window.prompt("Digita el número");

function invertirDigitos(numero)
{ 
	return Number(numero.toString().split('').reverse().join(''));
}


document.write("El número invertido es: " +invertirDigitos(num_original) + "  :)");




/*
Lab 5 ejercicio 6
Crea una solución para un problema de tu elección (puede ser algo relacionado con tus intereses, 
alguna problemática que hayas identificado en algún ámbito, un problema de programación que hayas resuelto 
en otro lenguaje, un problema de la ACM, entre otros). El problema debe estar descrito en un documento HTML, 
y la solución implementada en JavaScript, utilizando al menos la creación de un objeto, el objeto además de
 su constructor deben tener al menos 2 métodos. Muestra los resultados en el documento HTML.
*/


function Animales(nombre, especie) {
    this.nombre = nombre;
    this.especie = especie;
}

Animales.prototype.ladra = function(){
    return `${this.nombre} puede ladrar`;
}

Animales.prototype.muerde = function() {
    return `${this.nombre} muerde a los desconocidos, ¡ten cuidado!`;
}

let perro = new Animales("Pol", "Perro");

document.write(perro.ladra()+ ".    ");

document.write(perro.muerde());



