const jugadores = [{nombre: "Leo Messi", equipo: "Paris Saint Germain"},
				   {nombre: "Cristiano Ronaldo", equipo:"Manchester united"},
				   {nombre: "Neymar Jr", equipo:"Paris Saint Germain"},
				   {nombre:"Erling Halaand", equipo:"Borussia Dortmund"} 
];

const requestHandler = (request, response) => {
	console.log(request.url);

	if(request.url === "/"){
		response.statusCode = 200;
        response.setHeader('Content-Type', 'text/html');
        response.write('<head><meta charset="UTF-8"></head>');
        response.write('<body style="background-color: #aee7f5; color: blue; text-align:center">');
        response.write('<h1>Bienvenidos al navegador fútbol</h1>');
        response.write('<br>');
        response.write('<h2 style="text-align: center; font-size: 50px">Elige una opción<h2>');
        response.write('<br><br>');
        response.write('<a ref ="/eventos" style="font-size: 25px">Próximos eventos futbolísticos</a>');
        response.write('<br><br>');
        response.write('<a href = "/noticias" style="font-size: 25px">Noticias sobre el fútbol</a>');
        response.write('<br><br>');
        response.write('<a href = "/mejoresjugadores" style="font-size: 25px">Mejores jugadores</a>');
        response.write('<br><br>');
        response.write('</body>');
        response.end();
	} 

	else if (request.url === "/eventos"){
		response.statusCode = 200;
		response.setHeader('Content-Type', 'text/html');
		response.write('<head><meta charset="UTF-8"></head>');
		response.write('<body style="background-color: #aee7f5; color: blue; text-align:center">');
		response.write('<h2>Eventos próximos</h2>');
		response.write('<br>');
		response.write('<img style="width: 50%" src="https://cnnespanol.cnn.com/wp-content/uploads/2019/09/190903171331-qatar-world-cup-emblem-exlarge-169.jpg?quality=100&strip=info">');
		response.write('<br>');
		response.write('<a ref ="/" style="font-size: 25px">Regresar</a>');
		response.write('<br><br>');
        response.write('</body>');
        response.end();
	}

	else if (request.url === "/noticias"){
		response.statusCode = 200;
		response.setHeader('Content-Type', 'text/html');
		response.write('<head><meta charset="UTF-8"></head>');
		response.write('<body style="background-color: #aee7f5; color: blue; text-align:center">');
		response.write('<h2>Noticias recientes</h2>');
		response.write('<br>');
		response.write('<a href="https://www.tvazteca.com/aztecadeportes/futbol-internacional/cuando-sera-el-debut-de-messi-en-la-champions-esp1">¿Cuándo será el debut de Leo Messi?</a>');
		response.write('<br>');
		response.write('<a href="https://www.eluniversal.com.mx/universal-deportes/futbol/cristiano-ronaldo-su-debut-con-el-manchester-united-no-se-transmitira">No se transmitirá el debut de CR7</a>');
		response.write('<br>');
		response.write('<a href="https://as.com/futbol/2021/09/09/primera/1631180151_802049.html">Halaand al Real Madrid</a>');
		response.write('<br>');
		response.write('<a href="/">Regresar</a>');
		response.write('<br><br>');
        response.write('</body>');
        response.end();
	}

	else if (request.url === "/mejoresjugadores") {
		response.statusCode = 200;
		response.setHeader('Content-Type', 'text/html');
		response.write('<head><meta charset="UTF-8"></head>');
		response.write('<body style="background-color: #aee7f5; color: blue; text-align:center">');
		response.write('<h2>Agrega a tu jugador favorito con su equipo</h2>');
		response.write('<ul>');
		for (let jugador of jogadores){
			response.write('<li>'+ jugador.nombre + ": " + jugador.equipo + '</li>');
		}
		response.write('</ul>');
		response.write('<a href="/mejoresjugadores/add">Agregar jugador</a>');
		response.write('<br><br>');
		response.write('<a href="/">Regresar</a>');
        response.write('</body>');
        response.end();
	}

	else if (request.url === "/mejoresjugadores/add" && request.method === "GET"){
		console.log(request.method);
		response.setHeader('Content-Type', 'text/html');
		response.write('<head><meta charset="UTF-8"></head>');
		response.write('<h1>Agregar a tu jugador favorito a la lista </h1>');
		response.write('<br>');
		response.write('<form action="/mejoresjugadores/add" method="POST">');
		response.write('<label for="nombre">Nombre del jugador: </label>');
		response.write('<input type="text" id="nombre" name="nombre" placeholder="Luis García" required style="margin-left:10px">');
		response.write('<br><br>');
		response.write('<label for="equipo">Equipo de fútbol: </label>');
		response.write('<input type="text" id="equipo" name="equipo" placeholder="Manchester City">');
		response.write('<br><br>');
		response.write('<input type="submit" id="enviar" name="enviar" value="Enviar">');
        response.write('</form>');
        response.write('<br>');
        response.write('<a href = "/" style="font-size: 25px">Regresar a Inicio</a>');
        response.write('</body>');
        response.end();
	}

	else if (request.url === "/mejoresjugadores/add" && request.method === "POST"){
		console.log(request.method);
		const datos = [];
		//Recibir datos del cliente
		request.on('data', (dato) => {
			//console.log(dato);
			datos.push(dato);
		});
		//Procesar datos del cliente
       return request.on('end', () => {
            const datos_completos = Buffer.concat(datos).toString();
            console.log(datos);
            console.log(datos_completos);
            const nombre = datos_completos.split('=')[1].split('&')[0];
            const equipo = datos_completos.split('=')[2].split('&')[0];
            console.log(nombre);
            console.log(equipo);
            
            //Agregar el jugador
            jugadores.push({nombre: nombre, equipo: equipo});

            //Agregar a archivo
            let fs = require('fs')
            let logger = fs.createWriteStream('futbolistas.txt', {
            flags: 'a'
            });

            logger.write("nombre: " + nombre + ", equipo: " +  equipo + "\n"); // append string to your file


            //Redireccionar hacia /mejoresjugadores
            response.statusCode = 302;
            response.setHeader('Location', '/mejoresjugadores');
            response.end();
        });
	}

	else {
		response.statusCode = 404;
        response.setHeader('Content-Type', 'text/html');
        response.write('<head><meta charset="UTF-8"></head>');
        response.write('<h1>Esta página no existe...</h1>');
        response.end();
	}
}

//Exportar la variable
module.exports = requestHandler;