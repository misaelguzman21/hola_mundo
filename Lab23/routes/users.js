const express = require('express');

const isAuth = require('../util/is-auth.js');


const router = express.Router();

const usersController = require('../controllers/users_controller');

router.get('/login', usersController.getLogin);

router.post('/login', usersController.postLogin);

router.get('/logout', isAuth, usersController.getLogout);

router.get('/add', isAuth, usersController.getAdd);

router.post('/add', isAuth, usersController.postAdd);

module.exports = router;