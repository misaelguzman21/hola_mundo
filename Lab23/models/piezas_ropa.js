const db = require('../util/database');


module.exports = class Pieza {

    //Constructor de la clase. Sirve para crear un nuevo objeto, y en él se definen las propiedades del modelo
    constructor(mi_nombre, mi_precio, mi_imagen) {
        this.nombre = mi_nombre;
        this.precio = mi_precio;
        this.imagen = mi_imagen;
    }

    //Este método servirá para guardar de manera persistente el nuevo objeto. 
    save() {
        return db.execute('INSERT INTO piezas (nombre, precio, imagen) VALUES (?, ?, ?)',
        [this.nombre, this.precio, this.imagen]);
        //piezas.push(this);
    }

    //Este método servirá para devolver los objetos del almacenamiento persistente.
    static fetchAll(id) {

        if(id === undefined){
            return db.execute(' SELECT * FROM piezas');  
        } else{
            return db.execute(' SELECT * FROM piezas WHERE id = ?', [id]); 
        }
        
        //return piezas;
        
    }

    static find(query) {
        return db.execute('SELECT * FROM piezas WHERE nombre LIKE ? OR precio LIKE ?', ['%'+query+'%', '%'+query+'%']);
    }

}