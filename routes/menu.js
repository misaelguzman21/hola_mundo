const express = require('express');

const router = express.Router();

const menuController = require('../controllers/menu_controller');

router.get('/list/:platillo_id', menuController.getListId);

router.get('/list', menuController.getList);

router.get('/add', menuController.getAdd);

router.post('/add', menuController.postAdd);

router.post('/search', menuController.postSearch)

module.exports = router;