const express = require('express');
const app = express();


const bodyParser = require('body-parser');
const rutasMenu = require('./routes/menu');
const rutasFutbol = require('./routes/futbol');



app.use(bodyParser.urlencoded({extended: false}));

app.use('/futbol', rutasFutbol);
app.use('/menu', rutasMenu);


app.use((request, response, next) => {
    console.log('Segundo middleware!');
    response.status(404).send('Recurso no encontrado :('); //Manda la respuesta
});

app.listen(3000);