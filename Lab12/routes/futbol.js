
const express = require('express');

const router = express.Router();

router.get('/eventos', (request, response, next) => {
    let evento = '<head><meta charset="UTF-8"></head>';
    evento += '<body style="background-color: #fce4ca; color: black; text-align:center">';
    evento += '<h2>Eventos próximos</h2>';
    evento += '<br>';
    evento += '<img style="width: 50%" src="https://cnnespanol.cnn.com/wp-content/uploads/2019/09/190903171331-qatar-world-cup-emblem-exlarge-169.jpg?quality=100&strip=info">';
    evento += '<br>';
    evento += '</body>';
    response.send(evento);
});

router.get('/noticias', (request, response, next) => {
    let noticia = '<head><meta charset="UTF-8"></head>';
    noticia += '<body style="background-color: #fce4ca; color: black; text-align:center">';
    noticia += '<br>';
    noticia += '<h2>Noticias recientes</h2>';
    noticia += '<br>';
    noticia += '<a href="https://www.tvazteca.com/aztecadeportes/futbol-internacional/cuando-sera-el-debut-de-messi-en-la-champions-esp1">¿Cuándo será el debut de Leo Messi?</a>';
    noticia += '<br>';
    noticia += '<a href="https://www.eluniversal.com.mx/universal-deportes/futbol/cristiano-ronaldo-su-debut-con-el-manchester-united-no-se-transmitira">No se transmitirá el debut de CR7</a>';
    noticia += '<br>';
    noticia += '<a href="https://as.com/futbol/2021/09/09/primera/1631180151_802049.html">Halaand al Real Madrid</a>';
    noticia += '<br>';
    noticia += '</body>';
    response.send(noticia);
});



router.get('/add', (request, response, next) => {
    let respuesta = '<head><meta charset="UTF-8"></head>';
    respuesta += '<h1>Agregar a tu jugador favorito a la lista </h1>';
    respuesta += '<br>';
    respuesta += '<form action="/futbol/add" method="POST">';
    respuesta += '<label for="nombre">Nombre del jugador: </label>';
    respuesta += '<input type="text" id="nombre" name="nombre" placeholder="Luis García" required style="margin-left:10px">';
    respuesta += '<br/>';
    respuesta += '<br/>';
    respuesta += '<label for="equipo">Equipo de fútbol: </label>';
    respuesta += '<input type="text" id="equipo" name="equipo" placeholder="Manchester City">';
    respuesta += '<br/>';
    respuesta += '<br/>';
    respuesta += '<input type="submit" id="enviar" name="enviar" value="Enviar">';
    respuesta +='</form>';
    response.send(respuesta);
});

router.post('/add', (request, response, next) => {
    let respuesta = '<head><meta charset="UTF-8"></head>';
    respuesta += '<body style="background-color: #fce4ca; color: blue; text-align:center">';
    respuesta += '<br>';
    respuesta += '<h2 style="text-align:center; font-size:100px">¡Futbolista fue introducido con éxito!</h2>';
    respuesta += '</body>';
    response.send(respuesta);
    let fs = require('fs')
    let logger = fs.createWriteStream('futbolistas.txt', {
    });
    logger.write("nombre: " + request.body.nombre + ", equipo: " +  request.body.equipo + "\n");
});

module.exports = router;