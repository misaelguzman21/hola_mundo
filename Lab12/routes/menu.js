const express = require('express');

const router = express.Router();


router.get('/pregunta', (request, response, next) => {
    let pregunta = '<head><meta charset="UTF-8"></head>';
    pregunta += '<body style="background-color: green; color: black; text-align:center">';
    pregunta += '<br>';
    pregunta += '<h1>Preguntas</h1>';
    pregunta += '<h3>Describe el archivo package.json.</h3>';
    pregunta += '<span>Archivo de definición o manifiesto para nuestro proyecto, en el cual especificamos referencias al proyecto como: autor, repositorio, versión y sobre todo las dependencias, entre otros.</span>';
    pregunta += '<br><br>';
    pregunta += '</body>';
    response.send(pregunta);
});

module.exports = router;