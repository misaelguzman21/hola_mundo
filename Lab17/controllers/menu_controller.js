const Pieza = require('../models/piezas_ropa');


exports.getList = (request, response, next) => {
    console.log('Ruta /menu/ropa');
    //console.log(request.get('Cookie').split(';')[1].trim().split('=')[1]);
    console.log(request.cookies);
    console.log(request.cookies.ultima_pieza);
    response.render('lista_ropa', {
        titulo: "Ropa",
        isLoggedIn: request.session.isLoggedIn,
        username: request.session.username,
        lista_piezas: Pieza.fetchAll(),
    });
};

exports.getAdd = (request, response, next) => {
    response.render('add_ropa',{
        isLoggedIn: request.session.isLoggedIn,
        username: request.session.username,
        titulo: "Agregar nueva pieza (sueter)",
    });
};

exports.postAdd = (request, response, next) => {
    console.log(request.body.nombre);
    response.setHeader('Set-Cookie', 'ultima_pieza= '+ request.body.nombre+'; HttpOnly');
    const pieza = new Pieza(request.body.nombre, request.body.precio, "https://m.media-amazon.com/images/I/61GnLXLVOkL._AC_SY550_.jpg" );
    pieza.save();
    response.status(302).redirect('/menu/ropa');
    
};