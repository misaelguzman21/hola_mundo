let piezas = [
    {nombre: "Capa Larga", precio: "$450", imagen: "../capa.png"},
    {nombre: "Suéter Dopo", precio: "$280", imagen: "../azul.png"},
    {nombre: "Saco Gris Chenille", precio: "$330", imagen:"../saco.png" }
];

module.exports = class Pieza {

    //Constructor de la clase. Sirve para crear un nuevo objeto, y en él se definen las propiedades del modelo
    constructor(mi_nombre, mi_precio, mi_imagen) {
        this.nombre = mi_nombre;
        this.precio = mi_precio;
        this.imagen = mi_imagen;
    }

    //Este método servirá para guardar de manera persistente el nuevo objeto. 
    save() {
        piezas.push(this);
    }

    //Este método servirá para devolver los objetos del almacenamiento persistente.
    static fetchAll() {
        return piezas;
        
    }

}