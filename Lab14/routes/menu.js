const express = require('express');

const router = express.Router();

const piezas = [
    {nombre: "Capa Larga", precio: "$450", imagen: "../capa.png"},
    {nombre: "Suéter Dopo", precio: "$280", imagen: "../azul.png"},
    {nombre: "Saco Gris Chenille", precio: "$330", imagen:"../saco.png" }
];

router.get('/ropa', (request, response, next) => {
    console.log('Ruta menu/ropa')
    response.render('lista_ropa', {
        productos: "Productos nuevos xD",
        lista_piezas: piezas
    
    });
});

router.get('/eventos', (request, response, next) => {
    let evento = '<head><meta charset="UTF-8"></head>';
    evento += '<body style="background-color: #fce4ca; color: black; text-align:center">';
    evento += '<h2>Eventos próximos</h2>';
    evento += '<br>';
    evento += '<img style="width: 50%" src="https://cnnespanol.cnn.com/wp-content/uploads/2019/09/190903171331-qatar-world-cup-emblem-exlarge-169.jpg?quality=100&strip=info">';
    evento += '<br>';
    evento += '</body>';
    response.send(evento);
});

router.get('/pregunta', (request, response, next) => {
    let pregunta = '<head><meta charset="UTF-8"></head>';
    pregunta += '<body style="background-color: green; color: black; text-align:center">';
    pregunta += '<br>';
    pregunta += '<h1>Preguntas</h1>';
    pregunta += '<h3>Describe el archivo package.json.</h3>';
    pregunta += '<span>Archivo de definición o manifiesto para nuestro proyecto, en el cual especificamos referencias al proyecto como: autor, repositorio, versión y sobre todo las dependencias, entre otros.</span>';
    pregunta += '<h3>¿Qué otros templating engines existen para node?</h3>';
    pregunta += '<span>Pug, Haml, ejs, hbs, blade, react, whiskers, etc.</span>';
    pregunta += '<br><br>';
    pregunta += '</body>';
    response.send(pregunta);
});

module.exports = router;