const Zombie = require('../models/zombie');




/*exports.getList = (request, response, next) => {
    console.log('Ruta /');
    //console.log(request.get('Cookie').split(';')[1].trim().split('=')[1]);
    console.log(request.cookies);
    //console.log(request.cookies.ultima_pieza)
    Zombie.fetchAll(request.params.zombie_id)
        .then(([rows, fieldData]) => {
            response.render('lista_zombies', {
            titulo: "Lista de zombies",
            lista_zombie: rows,fieldData
        });
        }).catch(err => {
            console.log(err);
            response.status(302).redirect('/error');
        });
};
*/

exports.getList = (request, response) => {
    request.getConnection((err, conn) => {
        conn.query('SELECT zombie.nombre, estados.fecha, estados.tipo_estado FROM zombie INNER JOIN estados ON zombie.idEstado = estados.id_estado', (err, zombies) =>{
            if(err){
                response.json(err);
            }
            console.log(zombies)
            response.render('lista_zombies', {
                titulo: "Lista de zombies",
                lista_zombie: zombies,
            })
        
    });
    
});
};

exports.getAdd = (request, response, next) => {
    response.render('registrar_estado',{
        titulo: "Registrar nuevo zombie:",
    });
};

exports.postAdd = (request, response) => {
    console.log(request.body);
    //response.setHeader('Set-Cookie', 'ultimo_registro= '+ request.body.nombre+'; HttpOnly');
    const zombie = new Zombie(request.body.nombre, request.body.tipo_estado, request.body.fecha );
    zombie.save()
    .then(() => {
        response.status(302).redirect('/');
        
    }).catch(err => {

        response.status(302).redirect('/error');
    });
    
};