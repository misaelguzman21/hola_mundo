const db = require('../util/database');

/*const platillos = [
    {nombre: "sopes", descripcion: "Tortilla, frijol, queso, salsa, pollo"},
    {nombre: "chilaquiles", descripcion: "Tortilla, salsa, frijol, queso, pollo"},
    {nombre: "tacos", descripcion: "Tortilla, carne, salsa"},
    {nombre: "pambazo", descripcion: "Bolillo con carne remojado en salsa"},
    {nombre: "carne asada", descripcion: "al carbón", imagen: "https://dam.cocinafacil.com.mx/wp-content/uploads/2019/06/cow-boy.jpg"},
    {nombre: "hamburguesa", descripcion: "Carne entre 2 panes", imagen: "https://www.hola.com/imagenes/cocina/noticiaslibros/20210528190401/dia-internacional-hamburguesa-recetas-2021/0-957-454/dia-hamburguesa-m.jpg"}
]; */


module.exports = class Zombie {

    //Constructor de la clase. Sirve para crear un nuevo objeto, y en él se definen las propiedades del modelo
    constructor(mi_nombre, mi_tipo_estado) {

        this.nombre = mi_nombre;
        this.tipo_estado = mi_tipo_estado;
        this.fecha = null;
        
    }

    //Este método servirá para guardar de manera persistente el nuevo objeto. 
    save() {
        return db.execute('INSERT INTO zombie (nombre, tipo_estado, fecha) VALUES ( ?, ?, ?)',
        [ this.nombre, this.tipo_estado, this.fecha]);
    }

    //Este método servirá para devolver los objetos del almacenamiento persistente.
    static fetchAll(id_estado) {
        if (id_estado) {
            return db.execute('SELECT zombie.nombre, estados.fecha, estados.tipo_estado FROM zombie INNER JOIN estados ON zombie.idEstado = estados.id_estado');
        } else{
            return db.execute('SELECT zombie.nombre, estados.fecha, estados.tipo_estado FROM zombie INNER JOIN estados ON zombie.idEstado = estados.id_estado  WHERE id_estado = ?', [id_estado]);

        }
    
    }

}