const express = require('express');
const app = express();


const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');

const rutasMenu = require('./routes/menu');
const rutasUsers = require('./routes/users');


const path = require('path');
const router = express.Router();

//Middleware
app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());

app.use(session({
    secret: 'JUDHUBDUA8337Gghvsg72bwb7WUB2E7JNB83UMKa82', //'mi string secreto que debe ser un string aleatorio muy largo, no como éste', 
    resave: false, //La sesión no se guardará en cada petición, sino sólo se guardará si algo cambió 
    saveUninitialized: false, //Asegura que no se guarde una sesión para una petición que no lo necesita
}));


app.get('/index', (request, response, next) => {
    response.sendFile(path.join(__dirname, 'views', 'index1.html'));
});



app.use('/menu', rutasMenu)
app.use('/users', rutasUsers)

app.use((request, response, next) => {
    console.log('Primer Middleware!');
    next(); //Le permite a la petición avanzar hacia el siguiente middleware
});


app.use('/ruta/nombres',(request, response, next) => {
    console.log('Tercer middleware!');
    response.send('Respuesta de la ruta "/ruta/nombres"'); //Manda la respuesta
   
});

app.use('/ruta/apellidos',(request, response, next) => {
    console.log('Tercer middleware!');
    response.send('Respuesta de la ruta "/ruta/apellidos"'); //Manda la respuesta
   
});

app.use('/ruta',(request, response, next) => {
    console.log('Segundo middleware!');
    response.send('Respuesta de la ruta "/ruta"'); //Manda la respuesta
   
});

app.use('/error',(request, response, next) => {
    console.log('Segundo middleware!');
    response.status(500).send('Internal server error'); //Manda la respuesta
   
});


app.use((request, response, next) => {
    console.log('Segundo middleware!');
    response.status(404).send('Recurso no encontrado'); //Manda la respuesta
});



app.listen(3000);